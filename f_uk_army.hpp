// garand                     rm
// garand + rifle grenade     gr
// carbine + M1 bazooka       lat
// thompson                   hp
// carbine                    hc
// BAR                        dm
// 1919                       ar


class uk_army_af
{
  name = "WW2: UK Army - Africa";

  class base
  {
    uniform[] = {
      U_LIB_UK_KhakiDrills,
    };
    helmet[] = {
      H_LIB_UK_Helmet_Mk2,
      H_LIB_UK_Helmet_Mk2_Cover,
      H_LIB_UK_Helmet_Mk2_Camo,
      H_LIB_UK_Helmet_Mk2_Desert,
      H_LIB_UK_Helmet_Mk2_Net
    };
  };

  class rm : base // Enfield
  {
    vest[] = { V_LIB_UK_P37_Rifleman_Blanco };
    pack[] = { B_LIB_UK_HSack_Blanco_Cape };
  };
  class gr : base // Enfield + grenades
  {
    vest[] = { V_LIB_UK_P37_Rifleman_Blanco };
    pack[] = { B_LIB_UK_HSack_Blanco_Cape };
  };
  class lat : base // Enfield + M1
  {
    vest[] = { V_LIB_UK_P37_Rifleman_Blanco };
    pack[] = { fow_b_us_rocket_bag };
  };
  class ar : base // Bren
  {
    vest[] = { V_LIB_UK_P37_Heavy_Blanco };
    pack[] = { B_LIB_UK_HSack_Blanco_Cape };
  };
  class aar : base // Enfield + Bren ammo
  {
    vest[] = { V_LIB_UK_P37_Heavy_Blanco };
    pack[] = { B_LIB_UK_HSack_Blanco_Cape };
  };
  class cls : base // Sten + medkit
  {
    helmet[] = { fow_h_us_m1_medic };
    vest[] = { V_LIB_UK_P37_Heavy_Blanco };
    pack[] = { B_LIB_SOV_RA_MedicalBag_Empty };
  };

  class eng : base // Sten + boom?
  {
    vest[] = { V_LIB_UK_P37_Sten_Blanco };
    pack[] = { B_LIB_UK_HSack_Blanco_Cape };
  };
  class dm : base // Enfield
  {
    vest[] = { V_LIB_UK_P37_Rifleman_Blanco };
    pack[] = { B_LIB_UK_HSack_Blanco_Cape };
  };

  class hp : base // Sten
  {
    vest[] = { V_LIB_UK_P37_Sten_Blanco };
    pack[] = { B_LIB_UK_HSack_Blanco_Cape };
  };

  class hc : hp // Enfield
  {
    vest[] = { V_LIB_UK_P37_Sten_Blanco };
    pack[] = { B_LIB_UK_HSack_Blanco_Cape };
  };

  class co : rm
  {
    binocular = LIB_Binocular_UK;
  };

  class tl : co
  {
  };

  class fac : co
  {
    vest[] = { fow_v_us_carbine };
    pack[] = { fow_b_us_radio };
  };

  class sl : co
  {
  };

  class vc : base
  {
    helmet[] = { H_LIB_UK_Beret_Headset };
    binocular = LIB_Binocular_UK;
    vest[] = { V_LIB_UK_P37_Crew };
    pack[] = { B_LIB_UK_HSack_Blanco_Cape };
  };
};
