// garand                     rm
// garand + rifle grenade     gr
// carbine + M1 bazooka       lat
// thompson                   hp
// carbine                    hc
// BAR                        dm
// 1919                       ar


class us_army_eu
{
  name = "WW2: US Army - Europe";

  class base
  {
    uniform[] = {
      fow_u_us_m41_04_private,
      fow_u_us_m41_01_private,
      fow_u_us_m41_02_private
    };
    helmet[] = {
      fow_h_us_m1,
      fow_h_us_m1_closed,
      fow_h_us_m1_folded,
      fow_h_us_m2_net,
      fow_h_us_m2
    };
  };

  class rm : base // garand
  {
    vest[] = { fow_v_us_garand_bandoleer };
    pack[] = { fow_b_us_m1928 };
  };
  class gr : base // garand + rifle grenade
  {
    vest[] = { fow_v_us_grenade };
    pack[] = { fow_b_us_m1928 };
  };
  class lat : base // carbine + M1
  {
    vest[] = { fow_v_us_garand_bandoleer };
    pack[] = { fow_b_us_rocket_bag };
  };
  class ar : base // 1919
  {
    vest[] = { fow_v_us_asst_mg };
    pack[] = { fow_b_us_m1944 };
  };
  class aar : base // carbine + 1919 ammo
  {
    vest[] = { fow_v_us_asst_mg };
    pack[] = { fow_b_us_m1944 };
  };
  class cls : base // carbine + medkit
  {
    helmet[] = { fow_h_us_m1_medic };
    vest[] = { fow_v_us_medic };
    pack[] = { B_LIB_SOV_RA_MedicalBag_Empty };
  };

  class eng : base // garand + boom?
  {
    vest[] = { fow_v_us_carbine_eng };
    pack[] = { fow_b_us_m1928 };
  };
  class dm : base // BAR
  {
    vest[] = { fow_v_us_bar };
    pack[] = { fow_b_us_m1928 };
  };

  class hp : base // thompson
  {
    vest[] = { fow_v_us_thompson };
    pack[] = { fow_b_us_m1928 };
  };

  class hc : hp // carbine
  {
    vest[] = { fow_v_us_carbine };
    pack[] = { fow_b_us_bandoleer };
  };

  class co : rm
  {
    binocular = LIB_Binocular_US;
  };

  class tl : co
  {
  };

  class fac : co
  {
    vest[] = { fow_v_us_carbine };
    pack[] = { fow_b_us_radio };
  };

  class sl : co
  {
  };

  class vc : base
  {
    helmet[] = { H_LIB_US_Helmet_Tank };
    binocular = LIB_Binocular_US;
    vest[] = { fow_v_us_thompson };
    pack[] = { fow_b_us_bandoleer };
  };
};
