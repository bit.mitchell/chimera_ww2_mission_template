class ger_army
{
  name = "WW2: German Army";

  resupply[] = {
    {fow_w_pzfaust_100, 5},
    {ACE_fieldDressing, 12},
    {ACE_elasticBandage, 12},
    {ACE_packingBandage, 12},
    {ACE_splint, 4},
    {BIT_AidKit, 2},
    {fow_10nd_792x57, 40}, // g43
    {fow_5Rnd_792x57, 40}, // k98
    {fow_30Rnd_792x33, 20}, // stg44
    {fow_50Rnd_792x57, 20}, // mg42
    {fow_32Rnd_9x19_mp40, 20}, // mp40
    {fow_e_m24, 10},
    {fow_e_nb39b, 10},
    {fow_e_m24_at, 10}
  };

  medical[] = {
    {BIT_AidKit, 5},
    {ACE_salineIV, 10},
    {ACE_salineIV_500, 20},
    {ACE_bandage, 50},
    {ACE_quikclot, 50},
    {ACE_elasticBandage, 30},
    {ACE_packingBandage, 30},
    {ACE_morphine, 20},
    {ACE_epinephrine, 20},
    {ACE_adenosine, 20},
    {ACE_tourniquet, 10},
    {ACE_splint, 10}
  };


  class rm // G43
  {
    primary[] = {
      {fow_w_g43, "", "", "", ""}
    };

    items[] = {
      FAK,
      {"fow_10nd_792x57", 15},
      {"fow_e_m24", 2},
      {"fow_e_nb39b", 2}
    };

    pack[] = {};
  };

  class gr : rm // G43 + rifle grenades
  {
    items[] = {
      FAK,
      {"fow_10nd_792x57", 10},
      {"fow_e_m24", 6},
      {"fow_e_nb39b", 2}
    };
  };

  class lat : rm // K98 + panzer
  {
    primary[] = {
      {fow_w_k98, "", "", "", ""}
    };
    items[] = {
      FAK,
      {"fow_5Rnd_792x57", 15},
      {"fow_e_nb39b", 2},
      {"fow_e_m24_at", 5}
    };

    secondary = "fow_w_pzfaust_100";
  };

  class ar // MG42
  {
    primary[] = {{"fow_w_mg42", "", "", "", ""}};

    items[] = {
      FAK,
      {"fow_50Rnd_792x57", 6}
    };

    pack[] = {};
  };

  class aar // K98 + MG42 ammo
  {
    primary[] = {{"fow_w_k98", "", "", "", ""}};

    items[] = {
      FAK,
      {"fow_5Rnd_792x57", 15},
      {"fow_50Rnd_792x57", 5},
      {"fow_e_nb39b", 2}
    };

    pack[] = {};
  };

  class cls // K98 + medkit
  {
    medic = 1;
    primary[] = {{"fow_w_k98", "", "", "", ""}};

    items[] = {
      CLS,
      {"fow_5Rnd_792x57", 15}
    };
  };

  class eng : rm // K98 + boom
  {
    primary[] = {{"fow_w_k98", "", "", "", ""}};
    items[] = {
      FAK,
      ENG,
      {"fow_5Rnd_792x57", 10},
      {"fow_e_nb39b", 4},
      {"fow_e_tnt_onepound_mag", 3 },
    };
  };

  class dm // STG44
  {
    primary[] = {
      {"fow_w_stg44", "", "", "", ""}
    };

    items[] = {
      FAK,
      {"fow_30Rnd_792x33", 10},
      {"fow_e_m24", 2},
      {"fow_e_nb39b", 2}
    };

    pack[] = {};
  };

  class vc // MP40
  {
    primary[] = {{"fow_w_mp40", "", "", "", ""}};
    items[] = {
      FAK,
      {"fow_32Rnd_9x19_mp40", 8},
      {"fow_e_nb39b", 2}
    };
    pack[] = {};
  };

  class hp // MP40
  {
    primary[] = {{"fow_w_mp40", "", "", "", ""}};
    items[] = {
      FAK,
      {"fow_32Rnd_9x19_mp40", 8},
      {"fow_e_m24", 2}
    };
    pack[] = {};
  };

  class hc // G43
  {
    primary[] = {{"fow_w_g43", "", "", "", ""}};
    items[] = {
      FAK,
      {"fow_10nd_792x57", 14},
      {"fow_e_m24", 1},
    };
    pack[] = {};
  };

  class co : rm // G43
  {
  };

  class fac // K98 + radio
  {
    primary[] = {{"fow_w_k98", "", "", "", ""}};
    items[] = {
      FAK,
      {"ACRE_PRC148", 1},
      {"fow_5Rnd_792x57", 15},
      {"fow_e_nb39b", 2}
    };
  };

  class sl : co // G43
  {
  };

  class tl : co // G43
  {
  };
};