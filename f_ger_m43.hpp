// G43                        rm
// G43 + rifle grenade        gr
// k98 + panzer               lat
// mp40                       hp
// g43                        hc
// stg44                      dm
// mg42                       ar

class ger_m43
{
  name = "WW2: German Army - M43";

  class base
  {
    uniform[] = {
        fow_u_ger_m43_02_private,
        fow_u_ger_m43_01_private
    };
    helmet[] = {
        fow_h_ger_m40_heer_01,
        fow_h_ger_m40_heer_02,
        fow_h_ger_m42_heer_01
    };
  };

  class rm : base // G43
  {
    vest[] = {fow_v_heer_k98};
    pack[] = {B_LIB_GER_A_frame};
  };
  class gr : rm // G43 + rifle grenade
  {
  };
  class lat : base // k98 + panzer
  {
    vest[] = {fow_v_heer_k98};
    pack[] = {B_LIB_GER_SapperBackpack_empty};
  };
  class ar : base // mg42
  {
    vest[] = {fow_v_heer_mg};
    pack[] = {fow_b_heer_ammo_belt};
  };
  class aar : base // k98 + mg42 ammo
  {
    vest[] = {fow_v_heer_k98};
    pack[] = {fow_b_ammoboxes};
  };
  class cls : base // k98 + medkit
  {
    vest[] = {fow_v_heer_k98};
    pack[] = {fow_b_tornister_medic};
  };

  class eng : base // k98 + boom?
  {
    vest[] = {fow_v_heer_k98};
    pack[] = {B_LIB_GER_Backpack};
  };
  class dm : base // stg44
  {
    vest[] = {fow_v_heer_mp44};
    pack[] = {B_LIB_GER_A_frame};
  };

  class hp : base // mp40
  {
    vest[] = {fow_v_heer_mp40};
    pack[] = {B_LIB_GER_A_frame};
  };

  class hc : hp // g43
  {
    vest[] = {fow_v_heer_g43};
    pack[] = {B_LIB_GER_A_frame};
  };

  class co : rm
  {
    binocular = LIB_Binocular_GER;
  };

  class tl : co
  {
  };

  class fac : co
  {
    vest[] = {fow_v_heer_k98};
    pack[] = {B_LIB_GER_Radio_ACRE2};
  };

  class sl : co
  {
  };

  class vc : base
  {
    helmet[] = {fow_h_ger_m38_feldmutze_panzer};
    binocular = LIB_Binocular_GER;
    vest[] = {fow_v_heer_mp40};
    pack[] = {B_LIB_GER_A_frame_kit};
  };
};
