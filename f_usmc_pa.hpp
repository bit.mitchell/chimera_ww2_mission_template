// garand                     rm
// garand + rifle grenade     gr
// carbine + M1 bazooka       lat
// thompson                   hp
// carbine                    hc
// BAR                        dm
// 1919                       ar


class usmc_pa
{
  name = "WW2: US Marine Corp - Pacific";

  class base
  {
    uniform[] = {
      fow_u_usmc_m41_01_private,
      fow_u_usmc_p41_01_private,
      fow_u_us_m41_02_private
    };
    helmet[] = {
      fow_h_usmc_m1_camo_01,
      fow_h_usmc_m1_camo_02,
      fow_h_usmc_m1
    };
  };

  class rm : base // carbine
  {
    vest[] = { fow_v_usmc_carbine };
    pack[] = { fow_b_usmc_m1928 };
  };
  class gr : base // carbine + grenades
  {
    vest[] = { fow_v_usmc_carbine };
    pack[] = { fow_b_usmc_m1928 };
  };
  class lat : base // carbine + M1
  {
    vest[] = { fow_v_usmc_carbine };
    pack[] = { fow_b_us_rocket_bag };
  };
  class ar : base // 1919
  {
    vest[] = { fow_v_usmc_bar };
    pack[] = { fow_b_us_m1944 };
  };
  class aar : base // carbine + 1919 ammo
  {
    vest[] = { fow_v_usmc_bar };
    pack[] = { fow_b_us_m1944 };
  };
  class cls : base // carbine + medkit
  {
    helmet[] = { fow_h_us_m1_medic };
    vest[] = { fow_v_us_medic };
    pack[] = { B_LIB_SOV_RA_MedicalBag_Empty };
  };

  class eng : base // carbine + boom?
  {
    vest[] = { fow_v_usmc_carbine };
    pack[] = { fow_b_usmc_m1928 };
  };
  class dm : base // BAR
  {
    vest[] = { fow_v_usmc_bar };
    pack[] = { fow_b_usmc_m1928 };
  };

  class hp : base // thompson
  {
    vest[] = { fow_v_usmc_thompson };
    pack[] = { fow_b_usmc_m1928 };
  };

  class hc : hp // carbine
  {
    vest[] = { fow_v_usmc_carbine };
    pack[] = { fow_b_usmc_m1928 };
  };

  class co : rm
  {
    binocular = LIB_Binocular_US;
  };

  class tl : co
  {
  };

  class fac : co
  {
    vest[] = { fow_v_usmc_carbine };
    pack[] = { fow_b_us_radio };
  };

  class sl : co
  {
  };

  class vc : base
  {
    helmet[] = { H_LIB_US_Helmet_Tank };
    binocular = LIB_Binocular_US;
    vest[] = { fow_v_usmc_thompson };
    pack[] = { fow_b_us_bandoleer };
  };
};
