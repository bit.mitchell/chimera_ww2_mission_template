class usmc
{
  name = "WW2: US Marine Corp";

  resupply[] = {
    {LIB_1Rnd_60mm_M6, 5},
    {ACE_fieldDressing, 12},
    {ACE_elasticBandage, 12},
    {ACE_packingBandage, 12},
    {ACE_splint, 4},
    {BIT_AidKit, 2},
    {fow_15Rnd_762x33, 60}, // carbine
    {fow_50Rnd_762x63, 20}, // 1919
    {fow_20Rnd_762x63, 20}, // bar
    {fow_30Rnd_45acp, 20}, // thompson
    {fow_e_mk2, 10},
    {smokeshell, 10}
  };

  medical[] = {
    {BIT_AidKit, 5},
    {ACE_salineIV, 10},
    {ACE_salineIV_500, 20},
    {ACE_bandage, 50},
    {ACE_quikclot, 50},
    {ACE_elasticBandage, 30},
    {ACE_packingBandage, 30},
    {ACE_morphine, 20},
    {ACE_epinephrine, 20},
    {ACE_adenosine, 20},
    {ACE_tourniquet, 10},
    {ACE_splint, 10}
  };


  class rm // carbine
  {
    primary[] = {
      {fow_w_m1_carbine, "", "", "", ""}
    };

    items[] = {
      FAK,
      {"fow_15Rnd_762x33", 15},
      {"fow_e_mk2", 2},
      {"SmokeShell", 2}
    };

    pack[] = {};
  };

  class gr : rm // carbine + rifle grenades
  {
    items[] = {
      FAK,
      {"fow_15Rnd_762x33", 10},
      {"fow_e_mk2", 6},
      {"SmokeShell", 2}
    };
  };

  class lat : rm // carbine + M1
  {
    items[] = {
      FAK,
      {"fow_15Rnd_762x33", 8},
      {"SmokeShell", 2},
      {"LIB_1Rnd_60mm_M6", 2 }
    };

    secondary = "LIB_M1A1_Bazooka";
  };

  class ar // 1919
  {
    primary[] = {{"fow_w_m1919a6", "", "", "", ""}};

    items[] = {
      FAK,
      {"fow_50Rnd_762x63", 7},
      {"smokeshell", 2}
    };

    pack[] = {};
  };

  class aar // carbine + 1919 ammo
  {
    primary[] = {{"fow_w_m1_carbine", "", "", "", ""}};

    items[] = {
      FAK,
      {"fow_15Rnd_762x33", 8},
      {"fow_50Rnd_762x63", 5},
      {"smokeshell", 2}
    };

    pack[] = {};
  };

  class cls // carbine + medkit
  {
    medic = 1;
    primary[] = {{"fow_w_m1_carbine", "", "", "", ""}};

    items[] = {
      {"fow_15Rnd_762x33", 8},
      {"smokeshell", 2},
      CLS
    };
  };

  class eng : rm // carbine + boom
  {
    items[] = {
      FAK,
      ENG,
      {"fow_15Rnd_762x33", 8},
      {"smokeshell", 4},
      {"fow_e_tnt_onepound_mag", 2 },
    };
  };

  class dm // bar
  {
    primary[] = {
      {"fow_w_m1918a2", "", "", "fow_w_acc_m1918a2_bipod", ""},
      {"fow_w_m1918a2_bak", "", "", "fow_w_acc_m1918a2_bipod", ""}
    };

    items[] = {
      FAK,
      {"fow_20Rnd_762x63", 14},
      {"fow_e_mk2", 2},
      {"smokeshell", 2}
    };

    pack[] = {};
  };

  class vc // grease gun
  {
    primary[] = {{"fow_w_m3", "", "", "", ""}};
    items[] = {
      FAK,
      {"fow_30Rnd_45acp", 8},
      {"smokeshell", 2}
    };
    pack[] = {};
  };

  class hp // thompson
  {
    primary[] = {{"fow_w_m1a1_thompson", "", "", "", ""}};
    items[] = {
      FAK,
      {"fow_30Rnd_45acp", 12},
      {"fow_e_mk2", 2}
    };
    pack[] = {};
  };

  class hc // carbine
  {
    primary[] = {{"fow_w_m1_carbine", "", "", "", ""}};
    items[] = {
      FAK,
      {"fow_15Rnd_762x33", 16},
      {"fow_e_mk2", 2},
      {"smokeshell", 1}
    };
    pack[] = {};
  };

  class co : rm // rifleman
  {
  };

  class fac : hc // carbine + radio
  {
    items[] = {
      FAK,
      {"ACRE_PRC148", 1},
      {"fow_15Rnd_762x33", 12},
      {"SmokeShell", 2}
    };
  };

  class sl : co // rifleman
  {
  };

  class tl : co // rifleman
  {
  };
};