class uk_army
{
  name = "WW2: UK Army";

  resupply[] = {
    {LIB_1Rnd_89m_PIAT, 5},
    {ACE_fieldDressing, 12},
    {ACE_elasticBandage, 12},
    {ACE_packingBandage, 12},
    {ACE_splint, 4},
    {BIT_AidKit, 2},
    {LIB_10Rnd_770x56, 20}, // Lee Enfield
    {fow_32Rnd_9x19_sten, 20}, // Sten
    {LIB_30Rnd_770x56, 20}, // Bren
    {fow_e_mk2, 10},
    {smokeshell, 10}
  };

  medical[] = {
    {BIT_AidKit, 5},
    {ACE_salineIV, 10},
    {ACE_salineIV_500, 20},
    {ACE_bandage, 50},
    {ACE_quikclot, 50},
    {ACE_elasticBandage, 30},
    {ACE_packingBandage, 30},
    {ACE_morphine, 20},
    {ACE_epinephrine, 20},
    {ACE_adenosine, 20},
    {ACE_tourniquet, 10},
    {ACE_splint, 10}
  };


  class rm { // Lee Enfield
    primary[] = {{LIB_LeeEnfield_No4, "", "", "", ""}};

    items[] = {
      FAK,
      {"LIB_10Rnd_770x56", 16},
      {"fow_e_mk2", 2},
      {"SmokeShell", 2}
    };

    pack[] = {};
  };

  class gr : rm // Lee Enfield + grenades
  {
    items[] = {
      FAK,
      {"LIB_10Rnd_770x56", 8},
      {"fow_e_mk2", 8},
      {"SmokeShell", 2}
    };
  };

  class lat : rm // Lee Enfield + PIAT
  {
    items[] = {
      FAK,
      {"LIB_10Rnd_770x56", 8},
      {"SmokeShell", 2},
      {"LIB_1Rnd_89m_PIAT", 2 }
    };

    secondary = "LIB_PIAT";
  };

  class ar // Bren
  {
    primary[] = {{"LIB_Bren_Mk2", "", "", "", ""}};

    items[] = {
      FAK,
      {"LIB_30Rnd_770x56", 7},
      {"smokeshell", 2}
    };

    pack[] = {};
  };

  class aar // Lee Enfield + Bren Ammo
  {
    primary[] = {{LIB_LeeEnfield_No4, "", "", "", ""}};

    items[] = {
      FAK,
      {"LIB_10Rnd_770x56", 8},
      {"LIB_30Rnd_770x56", 5},
      {"smokeshell", 2}
    };

    pack[] = {};
  };

  class cls // Sten
  {
    medic = 1;
    primary[] = {{"fow_w_sten_mk2", "", "", "", ""}};

    items[] = {
      {"fow_32Rnd_9x19_sten", 8},
      {"smokeshell", 2},
      CLS
    };
  };

  class eng // Sten + boom
  {
    primary[] = {{"fow_w_sten_mk2", "", "", "", ""}};
    items[] = {
      FAK,
      ENG,
      {"fow_32Rnd_9x19_sten", 10},
      {"smokeshell", 4},
      {"fow_e_tnt_onepound_mag", 2 },
    };
  };

  class dm : rm
  {
  };

  class vc // sten
  {
    primary[] = {{"fow_w_sten_mk2", "", "", "", ""}};
    items[] = {
      FAK,
      {"fow_32Rnd_9x19_sten", 8},
      {"smokeshell", 2}
    };
    pack[] = {};
  };

  class hp : rm
  {
    primary[] = {{"fow_w_sten_mk2", "", "", "", ""}};
    items[] = {
      FAK,
      {"fow_32Rnd_9x19_sten", 8},
      {"smokeshell", 2}
    };
    pack[] = {};
  };

  class hc : hp
  {
  };

  class co : rm // rifleman
  {
  };

  class fac : hc // carbine + radio
  {
    primary[] = {{LIB_LeeEnfield_No4, "", "", "", ""}};
    items[] = {
      FAK,
      {"ACRE_PRC148", 1},
      {"LIB_10Rnd_770x56", 12},
      {"SmokeShell", 2}
    };
  };

  class sl : co // rifleman
  {
  };

  class tl : co // rifleman
  {
  };
};