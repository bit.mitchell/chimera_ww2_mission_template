# Usage:

1. Open the editor and choose a map.
2. Save your empty map as `clever_name` in the `MPMissions` category.
3. Download the latest template files from GitLab.
4. Go to `Documents/Arma 3/MPMissions/clever_name.map`
5. Overwrite folder contents with downloaded template files.
6. In the editor, re-open your map to update with the new files.
7. Choose your faction. Allies are GREEN, Axis is Blue. Delete the unwanted group including modules.
8. Move the start block from the bottom left corner of the map to wherever you want to start.
   Right click -> transform -> snap to surface is quite useful when moving the start block.
9. Configure the Loadout and Respawn modules in the start block.
10. Delete unused groups such as Anvil if they wont be used.

# Loadout Selection (Mostly obsolete for WW2):

You have the following options:

Faction selects the uniforms/vest/packs and general _theme_ of the players for the mission. Equipment selects the weapons the players will be given. Having these as separate options allows you to mix-and-match as needed to get the right feeling for your mission.

Optics - enables the 1x optics the weapons are configured for (2x for the marksman)
Suppressors - enables the use of suppressors (not all weapons have them)
Rails - enables the rail attachments, i.e. lasers (not all weapons have them)
E-Tools - adds an E-Tool to each soldiers inventory
HALO - gives each player an altimeter watch, moves their pack to their chest, and gives them a parachute
NVGs - adds NVGs to each player, the type is defined by the faction selected.

# Respawn Settings:

This module allows you to enable/disable the respawn button in the spectate view.
The delay is how long a player must wait before the button shows up.

# Faction Equipment

Because the roles defined in Bits n Bobs don't quite line up with WW2 and it is a _massive_ amount of work to make them adjustable, here is the mapping of weapons to roles for the different factions:

## US Army

| Role                     | Weapon                   |
|--------------------------|--------------------------|
| Platoon Lead             | Garand                   |
| Forward Air Control      | Carbine                  |
| Squad Lead               | Garand                   |
| Team Lead                | Garand                   |
| Combat Life Saver        | Carbine                  |
| Rifleman                 | Garand                   |
| Rifleman (LAT)           | Garand + Bazooka         |
| Grenadier                | Garand + Rifle Grenades  |
| Auto-Rifleman            | M1919                    |
| Auto-Rifleman Assistant  | Carbine + M1919 Ammo     |
| Designated Marksman      | BAR                      |
| Combat Engineer          | Garand + Demo            |
| Helicopter Pilot         | Thompson                 |
| Helicopter Crewman       | Carbine                  |
| Vehicle Crew             | Grease Gun               |

## USMC

| Role                     | Weapon                   |
|--------------------------|--------------------------|
| Platoon Lead             | Thompson                 |
| Forward Air Control      | Carbine                  |
| Squad Lead               | Thompson                 |
| Team Lead                | Carbine                  |
| Combat Life Saver        | Carbine                  |
| Rifleman                 | Carbine                  |
| Rifleman (LAT)           | Carbine + Bazooka        |
| Grenadier                | Carbine + Grenades       |
| Auto-Rifleman            | M1919                    |
| Auto-Rifleman Assistant  | Carbine + M1919 Ammo     |
| Designated Marksman      | BAR                      |
| Combat Engineer          | Carbine + Demo           |
| Helicopter Pilot         | Thompson                 |
| Helicopter Crewman       | Carbine                  |
| Vehicle Crew             | Grease Gun               |

## German Army

| Role                     | Weapon                   |
|--------------------------|--------------------------|
| Platoon Lead             | G43                      |
| Forward Air Control      | Kar 98 + Radio           |
| Squad Lead               | G43                      |
| Team Lead                | G43                      |
| Combat Life Saver        | Kar 98                   |
| Rifleman                 | G43                      |
| Rifleman (LAT)           | Kar 98 + Panzerfaust     |
| Grenadier                | G43 + Stick Grenades     |
| Auto-Rifleman            | Mg42                     |
| Auto-Rifleman Assistant  | Kar 98 + Mg42 Ammo       |
| Designated Marksman      | Stg44                    |
| Combat Engineer          | Kar 98 + Demo            |
| Helicopter Pilot         | Mp40                     |
| Helicopter Crewman       | G43                      |
| Vehicle Crew             | Mp40                     |

## British Army

| Role                     | Weapon                   |
|--------------------------|--------------------------|
| Platoon Lead             | Lee Enfield              |
| Forward Air Control      | Lee Enfield              |
| Squad Lead               | Lee Enfield              |
| Team Lead                | Lee Enfield              |
| Combat Life Saver        | Sten                     |
| Rifleman                 | Lee Enfield              |
| Rifleman (LAT)           | Lee Enfield + PIAT       |
| Grenadier                | Lee Enfield + Grenades   |
| Auto-Rifleman            | Bren                     |
| Auto-Rifleman Assistant  | Lee Enfield + Bren Ammo  |
| Designated Marksman      | Lee Enfield              |
| Combat Engineer          | Sten                     |
| Helicopter Pilot         | Sten                     |
| Helicopter Crewman       | Sten                     |
| Vehicle Crew             | Sten                     |
